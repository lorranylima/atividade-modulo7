package br.com.mentorama.Cadastro;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cadastroAlunos")
public class CadastroController {

    private final List<Lista> listas;
    private Integer id;

    public CadastroController() {
        this.listas = new ArrayList<>();
        Lista lista1 = new Lista(1, "Davi", 11);
        Lista lista2 = new Lista(2, "Giovanna", 17);
        Lista lista3 = new Lista(3, "Carolayne", 26);
        this.listas.add(lista1);
        this.listas.add(lista2);
        this.listas.add(lista3);

    }

    @GetMapping
    public List<Lista> findAll(@RequestParam(required = false) String lista) {
        if(lista != null) {
            return listas.stream()
                    .filter(lst -> lst.getLista().contains(lista))
                    .collect(Collectors.toList());
        }
        return listas;
    }
    @GetMapping("/{id}")
    public Lista findById(@PathVariable("id")Integer id){
        return this.listas.stream()
                .filter(lst -> Objects.equals(lst.getId(), id))
                .findFirst()
                .orElse(null);
    }
    @PostMapping
    public Integer add(@RequestBody final Lista lista) {
        if (lista.getId() == 0) {
            lista.setId(listas.size() + 1);

        }
        listas.add(lista);
        return lista.getId();

    }
    @PutMapping
    public void update(@RequestBody final Lista lista){
        listas.stream()
                .filter(lst -> Objects.equals(lst.getId(), lista.getId()))
                .forEach(lst -> lst.setLista(lista.getLista()));


    }
@DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Integer id){
    listas.removeIf(lst -> Objects.equals(lst.getId(), id));

    }
}